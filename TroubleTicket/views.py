# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import redirect
from django.db import connection

from django import forms

from django.http import HttpResponse
from django.http import Http404
from django.template import Context, loader

from models import Customer
from models import Mitarbeiter
from models import Ticket
from models import Ticketverlauf

from .forms import MitarbeiterForm, CustomerForm, TicketForm

def index(request):
    return render(request, 'index/index.html')

def kunden_details(request):
    return HttpResponse("Das hier wird die Kundenverwaltung.")
   
#def kundenliste(request):
#    kunden = Customer.objects.all()
#    t = loader.get_template('customers/kunden_uebersicht.html')
#    c = Context({'object_list': kunden})
#    return HttpResponse(t.render(c))


def mitarbeiterliste(request):
    cursor=connection.cursor()
    kunden2=cursor.execute("select id, MA_Kuerzel, MA_Name, MA_Geburtsdatum, MA_Durchwahl, MA_Email from TroubleTicket_mitarbeiter where MA_Aktiv=1")
    return render(request, 'mitarbeiter/mitarbeiter_uebersicht.html', {'object_list':kunden2.fetchall()})

#def mitarbeiter_new(request):
#    if request.method == "POST":
#        form = MitarbeiterForm(request.POST)
#        if form.is_valid():
#            mitarbeiter = form.save(commit=False)
#            mitarbeiter.save()
#            return redirect('mitarbeiterliste')
#    else:
#        form = MitarbeiterForm()
#    
#    return render(request, 'mitarbeiter/mitarbeiter_edit.html', {'form': form})

def mitarbeiter_edit(request,maid):
    current_mitarbeiter=Mitarbeiter.objects.get(id=maid)
    
    return HttpResponse(current_mitarbeiter)

def tools(request):
    return render(request, 'tools.html')


def mitarbeiter_detail(request, mitarbeiter_id):
    mitarbeiter = Mitarbeiter.objects.get(pk=mitarbeiter_id)
    tickets=Ticket.objects.filter(TIC_Mitarbeiter=mitarbeiter_id)
    return render(request, 'mitarbeiter/mitarbeiter_detail.html', {'mitarbeiter': mitarbeiter, 'tickets':tickets})

def ticketliste(request):
    tickets = Ticket.objects.all()
    t=loader.get_template('tickets/tickets_uebersicht.html')
    c=Context({'object_list': tickets})
    return HttpResponse(t.render(c))

def ticket_detail(request,TicketID):
    ticket=Ticket.objects.get(id=TicketID)
    ticketverlauf=Ticketverlauf.objects.filter(TCCOM_TIC_ID=TicketID)
    return render(request, 'tickets/ticket_detail.html', {'ticket': ticket, 'ticketverlauf':ticketverlauf})

def kundenliste2(request):
    cursor=connection.cursor()
    kunden2=cursor.execute("select id, KND_kurzbezeichnung, KND_Bezeichnung from TroubleTicket_customer")
    return render(request,'customers/kundenliste2.html', {'object_list':kunden2.fetchall()})

def ticketliste2(request):
    cursor1=connection.cursor()
    ticket=cursor1.execute("select t.Id, t.TIC_Kurzbezeichnung, c.KND_Kurzbezeichnung, p.PRIO_Text, m.MA_Name, t.TIC_Verrechenbar from TroubleTicket_ticket t join TroubleTicket_priorities p on t.TIC_Priority_id=p.PRIO_Number join TroubleTicket_mitarbeiter m on t.TIC_Mitarbeiter_ID=m.ID join TroubleTicket_customer c on t.TIC_Kunde_id=c.id")
    return render(request, 'tickets/ticketliste2.html', {'object_list':ticket.fetchall()})

def customer_new(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            Kundenkuerzel = form.cleaned_data['Kundenkuerzel']
            Kundenname = form.cleaned_data['Kundenname']
            Customer.objects.create(KND_Kurzbezeichnung=Kundenkuerzel, KND_Bezeichnung=Kundenname)
            return redirect('kundenliste2')
    else:
        form=CustomerForm()
            
    return render(request, 'customers/customer_edit.html', {'form': form})

def mitarbeiter_new(request):
    if request.method == "POST":
        form = MitarbeiterForm(request.POST)
        if form.is_valid():
            MA_Kuerzel = form.cleaned_data['MA_Kuerzel']
            MA_Name = form.cleaned_data['MA_Name']
            MA_Geburtsdatum=form.cleaned_data['MA_Geburtsdatum']
            MA_Email=form.cleaned_data['MA_Email']
            MA_Durchwahl=form.cleaned_data['MA_Durchwahl']
            Mitarbeiter.objects.create(MA_Kuerzel=MA_Kuerzel, MA_Name=MA_Name, MA_Geburtsdatum=MA_Geburtsdatum, MA_Email=MA_Email, MA_Durchwahl=MA_Durchwahl, MA_Aktiv=1)
            return redirect('mitarbeiterliste')
    else:
        form=MitarbeiterForm()
            
    return render(request, 'mitarbeiter/mitarbeiter_edit.html', {'form': form})

def kunde_detail(request,KundeID):
    kunde=Customer.objects.get(id=KundeID)
    return render(request, 'customers/kunde_detail.html', {'kunde': kunde})