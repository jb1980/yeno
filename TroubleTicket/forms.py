# -*- coding: utf-8 -*-
from django import forms
from .models import Mitarbeiter, Customer, Ticket
from django.forms import ModelForm

class MitarbeiterForm(forms.Form):
    MA_Kuerzel=forms.CharField(max_length=3, label="Mitarbeiter Kürzel", required=True, error_messages={'required': 'Bitte geben Sie ein Mitarbeiterkürzel ein!'})
    MA_Name=forms.CharField(max_length=60, label="Mitarbeiter Name", required=True, error_messages={'required': 'Bitte geben Sie den Namen des Mitarbeiters ein!'})
    MA_Geburtsdatum=forms.DateField(label="Geburtsdatum", required=False)
    MA_Durchwahl=forms.CharField(max_length=5, label="Durchwahl", required=False)
    MA_Email=forms.CharField(max_length=30, label="E-Mail", required=False)
        
class CustomerForm(forms.Form):
    Kundenkuerzel=forms.CharField(max_length=4, label="Kundenkürzel")
    Kundenname=forms.CharField(max_length=256, label="Name des Kunden")
    
class TicketForm(forms.Form):    
    Ticket_Verrechenbar=forms.BooleanField()

class DEPRECATED_MitarbeiterForm(ModelForm):
    class Meta:
            model = Mitarbeiter
            fields = ['MA_Kuerzel', 'MA_Name', 'MA_Email', 'MA_Durchwahl', 'MA_Geburtsdatum', 'MA_Aktiv']
