# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0014_ticket_tic_verrechenbar'),
    ]

    operations = [
        migrations.AddField(
            model_name='mitarbeiter',
            name='MA_Aktiv',
            field=models.BooleanField(default=1),
            preserve_default=True,
        ),
    ]
