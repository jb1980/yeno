# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0004_ticket_tic_mitarbeiter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mitarbeiter',
            name='MA_Kuerzel',
            field=models.CharField(unique=True, max_length=3),
            preserve_default=True,
        ),
    ]
