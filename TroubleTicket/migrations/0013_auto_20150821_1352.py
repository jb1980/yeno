# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0012_ticketverlauf'),
    ]

    operations = [
        migrations.CreateModel(
            name='Priorities',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('PRIO_Number', models.IntegerField()),
                ('PRIO_Text', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='TIC_Priority',
            field=models.ForeignKey(to='TroubleTicket.Priorities'),
            preserve_default=True,
        ),
    ]
