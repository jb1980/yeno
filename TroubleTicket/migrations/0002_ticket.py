# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TIC_Kurzbezeichnung', models.CharField(max_length=20)),
                ('TIC_Problembeschreibung', models.TextField(max_length=255)),
                ('TIC_Priority', models.IntegerField()),
                ('TIC_Kunde', models.ForeignKey(to='TroubleTicket.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
