# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0008_auto_20150804_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='mitarbeiter',
            name='MA_Durchwahl',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mitarbeiter',
            name='MA_Email',
            field=models.CharField(default=2, max_length=40),
            preserve_default=False,
        ),
    ]
