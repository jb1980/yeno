# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0011_mitarbeiter_ma_geburtsdatum'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticketverlauf',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('TCCOM_Timestamp', models.DateTimeField(auto_now=True)),
                ('TCCOM_Text', models.TextField(max_length=200)),
                ('TCCOM_Owner', models.ForeignKey(to='TroubleTicket.Mitarbeiter')),
                ('TCCOM_TIC_ID', models.ForeignKey(to='TroubleTicket.Ticket')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
