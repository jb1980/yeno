# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0010_auto_20150804_1657'),
    ]

    operations = [
        migrations.AddField(
            model_name='mitarbeiter',
            name='MA_Geburtsdatum',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
    ]
