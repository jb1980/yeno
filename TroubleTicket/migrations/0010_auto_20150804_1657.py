# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0009_auto_20150804_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mitarbeiter',
            name='MA_Durchwahl',
            field=models.CharField(max_length=10, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mitarbeiter',
            name='MA_Email',
            field=models.CharField(max_length=40, null=True),
            preserve_default=True,
        ),
    ]
