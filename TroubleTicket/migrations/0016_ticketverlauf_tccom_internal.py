# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0015_mitarbeiter_ma_aktiv'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticketverlauf',
            name='TCCOM_Internal',
            field=models.BooleanField(default=0),
            preserve_default=True,
        ),
    ]
