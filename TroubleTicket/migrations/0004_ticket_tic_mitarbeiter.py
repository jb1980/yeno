# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TroubleTicket', '0003_mitarbeiter'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='TIC_Mitarbeiter',
            field=models.ForeignKey(default=1, to='TroubleTicket.Mitarbeiter'),
            preserve_default=False,
        ),
    ]
