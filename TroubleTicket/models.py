from django.db import models

class Customer(models.Model):
    KND_Kurzbezeichnung = models.CharField(max_length=4)
    KND_Bezeichnung = models.CharField(max_length=60)

    def __str__(self):
        return self.KND_Kurzbezeichnung

class Mitarbeiter(models.Model):
    MA_Kuerzel = models.CharField(max_length=3, unique=True)
    MA_Name=models.CharField(max_length=60)
    MA_Durchwahl=models.CharField(max_length=10, null=True)
    MA_Email=models.CharField(max_length=40, null=True)
    MA_Geburtsdatum=models.DateField(null=True)
    MA_Aktiv=models.BooleanField(default=1)
    
    def __str__(self):
        return self.MA_Kuerzel

class Priorities(models.Model):
    PRIO_Number = models.IntegerField()
    PRIO_Text=models.CharField(max_length=20)
    
    def __str__(self):
        return self.PRIO_Text

class Ticket(models.Model):
    TIC_Kurzbezeichnung = models.CharField(max_length=20)
    TIC_Problembeschreibung = models.TextField(max_length=255)
    TIC_Priority = models.ForeignKey(Priorities)
    TIC_Kunde = models.ForeignKey(Customer)
    TIC_Mitarbeiter=models.ForeignKey(Mitarbeiter)
    TIC_Verrechenbar=models.BooleanField(default=0)
    
    def __str__(self):
        return self.TIC_Kurzbezeichnung

class Ticketverlauf(models.Model):
    TCCOM_TIC_ID=models.ForeignKey(Ticket)
    TCCOM_Owner=models.ForeignKey(Mitarbeiter)
    TCCOM_Timestamp=models.DateTimeField(auto_now=True)
    TCCOM_Text=models.TextField(max_length=200)
    TCCOM_Internal=models.BooleanField(default=0)
    
    def __str__(self):
        return self.TCCOM_Text
    
