from django.contrib import admin

from .models import Customer, Ticket, Mitarbeiter, Ticketverlauf

class CustomerAdmin(admin.ModelAdmin):
    fields = ['KND_Kurzbezeichnung', 'KND_Bezeichnung']
    list_display=('KND_Kurzbezeichnung', 'KND_Bezeichnung')

class TicketAdmin(admin.ModelAdmin):
    fields = ['TIC_Kurzbezeichnung', 'TIC_Problembeschreibung', 'TIC_Priority', 'TIC_Kunde', 'TIC_Mitarbeiter', 'TIC_Verrechenbar']
    list_display=('TIC_Kurzbezeichnung', 'TIC_Problembeschreibung', 'TIC_Priority', 'TIC_Kunde', 'TIC_Mitarbeiter', 'TIC_Verrechenbar')

class MitarbeiterAdmin(admin.ModelAdmin):
    fields = ['MA_Kuerzel', 'MA_Name', 'MA_Geburtsdatum', 'MA_Durchwahl', 'MA_Email', 'MA_Aktiv']
    list_display=('MA_Kuerzel', 'MA_Name', 'MA_Geburtsdatum', 'MA_Durchwahl', 'MA_Email', 'MA_Aktiv')

class TicketverlaufAdmin(admin.ModelAdmin):
    fields = ['TCCOM_TIC_ID', 'TCCOM_Owner', 'TCCOM_Text']
    list_display=('TCCOM_TIC_ID', 'TCCOM_Owner', 'TCCOM_Text')

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(Mitarbeiter, MitarbeiterAdmin)
admin.site.register(Ticketverlauf, TicketverlaufAdmin)
