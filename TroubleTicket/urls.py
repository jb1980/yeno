from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'^kundenliste',views.kundenliste, name='kundenliste'),
    url(r'^kundenverwaltung',views.kunden_details, name='kundenverwaltung'),
    url(r'^mitarbeiterliste', views.mitarbeiterliste, name='mitarbeiterliste'),
    url(r'^mitarbeiter_neu', views.mitarbeiter_new, name='mitarbeiter_new'),
    url(r'^mitarbeiter_edit/(?P<maid>.*)/$', views.mitarbeiter_edit, name='mitarbeiter_edit'),
    url(r'tools',views.tools, name='tools'),
    url(r'^mitarbeiter/(?P<mitarbeiter_id>.*)/mitarbeiter_detail.html$', views.mitarbeiter_detail, name='mitarbeiter_detail'),
    url(r'^ticket_detail/(?P<TicketID>.*)/$',views.ticket_detail,name='ticket_detail'),
    url(r'^kundenliste', views.kundenliste2, name='kundenliste2'),
    url(r'^ticketliste', views.ticketliste2, name='ticketliste2'),
    url(r'customer_new', views.customer_new, name='customer_new'),
    url(r'^kunde_detail/(?P<KundeID>.*)/$',views.kunde_detail,name='kunde_detail')
    ]